from dataObject import DataObject

class Team(DataObject):
	
	def __init__(self, game, data={}):
		super(Team, self).__init__(data=data)
		self.game = game
		self.entities = {};

	def getEntities(self):
		return self.entities

	def __str__(self):
		return "team " + str(self.data["teamid"])

	def hasPlayer(self):
		return self.data["userid"] != None

	def setPlayer(self, user):
		self.data["userid"] = user.getId()

	def isUser(self, user):
		return self.data["userid"] == user.getId()

	def hasAliveEntity(self):
		for e in self.entities.values():
			if not e.isDestroyed():
				return True
		return False

	def addEntity(self, e):
		self.entities[e["entityid"]] = e

	def isDefeated(self):
		return self.data["defeated"]

	def setDefeated(self):
		self.data["defeated"] = True




				
		
