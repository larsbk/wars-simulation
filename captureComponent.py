from component import Component
from warsexceptions import *
from twisted.python import log

class componentCapture(Component):
	"""
	The component that allows entities to capture other entities
	"""
	
	def __init__(self, parent, specs=dict()):
		super(componentCapture, self).__init__(parent, specs)
		parent.actions["capture"] = self.captureCommand

	def captureCommand(self, message):
		"""
		Called when received a command to capture.

		@type message: dict
		@param message: the command description
		@rtype dict
		@return a valid response
		"""
		targets = self.parent.getEntitiesAtMyPos()
		
		capt = []
		for target in targets:
			if not target.isDestroyed() and "capturable" in target.specs and target.specs["capturable"] == True:
				self.parent.subtractValue("time", 100)
				target.data["teamid"] = self.parent.data["teamid"]
				self.parent.game.newData(entities=[target, self.parent])
				return 
	
		raise ParamError(reponseinfo="No entities to capture!")

