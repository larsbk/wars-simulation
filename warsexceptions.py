from txjason.service import JSONRPCError

class WarsError(JSONRPCError):
	def __init__(self, **d):
		try:
			self.message = d["responsedetail"]
		except:
			self.message = ""
		self.data = d

	def getData(self):
		return self.data

	def __str__(self):
		return str(self.data)

class RequestError(WarsError):
	def __init__(self, **d):
		super(RequestError, self).__init__(**d)

class ProtocolError(RequestError):
	def __init__(self, **d):
		super(ProtocolError, self).__init__(**d)
		self.data["responsedetail"] = "protocol violation"

class InternalError(WarsError):
	def __init__(self, **d):
		super(InternalError, self).__init__(**d)
		self.data["responsedetail"] = "internal_error"

class DataError(RequestError):
	def __init__(self, **d):
		super(DataError, self).__init__(**d)
		self.data["responsedetail"] = "data"

class DataResourceError(DataError):
	def __init__(self, valueName, **d):
		super(DataResourceError, self).__init__(**d)
		self.data["responsedetail"] = "resource"
		self.data["resource"] = valueName

class ParamError(RequestError):
	def __init__(self, **d):
		super(ParamError, self).__init__(**d)
		self.data["responsedetail"] = "param"


