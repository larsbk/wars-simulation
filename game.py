#from sim import *
from sim import map
from sim.team import Team
from sim.entity import Entity
import copy
import random
import math
from warsexceptions import *
from twisted.internet import defer
from twisted.python import log
from txjason.service import JSONRPCError
from termcolor import colored
from functools import wraps

class TurnError(JSONRPCError):
	def __init__(self, **d):
		self.message = "It's not your turn!"

def synchronized(fn):
	@defer.inlineCallbacks
	@wraps(fn)
	def wrap(self, *args, **kwargs):
		#yield self.lock.acquire()
		res = yield defer.maybeDeferred(fn, self, *args, **kwargs)
		#self.lock.release()
		defer.returnValue(res)
	return wrap



class Game(object):
	"""
	This class represents a game in progress. One is created per game per server.
	It loads the state from the database and saves it back after performing
	the requested operations.
	"""
	
	def __init__(self):
		self.gameid = None
		self.all_entities = {}
		self.neutral_entities = {}
		self.onTurn = defer.Deferred()
		self.teams = {}

	def newData(self, **kwargs):
		pass

	def __str__(self):
		if self.gameid != None:
			return "game (%s)" % str(self.gameid)
		else:
			return "game (unknown)"

	def loadEntity(self, u):
		"""
		Add a entity to the local list of entities
		"""
		self.all_entities[u.data["entityid"]] = u
		try:
			self.teams[u["teamid"]].addEntity(u)
		except KeyError:
			self.neutral_entities[u["entityid"]] = u

	def loadTeam(self, t):
		self.teams[t["teamid"]] = Team(self, data=t)

	def loadMap(self, m):
		self.mapid = m.getMapid()
		self.map = m

	def loadHistory(self, h):
		self.history = h

	def loadTurn(self, t):
		self.turn = t

	def loadCreator(self, c):
		self.creator = c
	
	def loadEntityset(self, e):
		self.entityset = e

	def loadGameid(self, g):
		self.gameid = g


	
	def checkEndGame(self):
		"""
		Check if any players has lost and update the data to reflect this.
		"""

		log.msg("check endgame")
		numDefeated = 0

		for team in self.teams.values():
			if (not team.isDefeated() and (not team.hasAliveEntity())):
				team.setDefeated()
				log.msg(str(team) + " lost!")
			if team.isDefeated():
				numDefeated += 1

		if numDefeated >= (len(self.teams) - 1):
			return True
		else:
			return False
		
		
	@synchronized
	def join(self, teamid, user):
		"""
		Join a game
		"""
		log.msg("Join!")
		
		try:
			t = self.teams[teamid]
		except IndexError:
			raise JSONRPCError(message="invalid teamid")

		if t.hasPlayer():
			raise JSONRPCError(message="already joined")

		if not t.hasAliveEntity():
			raise JSONRPCError(message="no controllable entities")

		t.setPlayer(user)
		return t.getData()

	def currentTeam(self):
		return ( self.teams[ self.getTurn() ] )

	def endTurn(self, user):
		"""
		End current turn
		"""

		log.msg("end turn %d" % self.getTurn())

		if not self.currentTeam().isUser(user):
			raise TurnError()

		gameOver = False
		
		while True:
			self.turn += 1

			for u in self.all_entities.itervalues():
				u.newRound()
				if self.getTurn() == 1:
					u.newDay()

			gameOver = self.checkEndGame()
			log.msg(gameOver, self.currentTeam(), self.currentTeam().isDefeated());
			if gameOver or (not self.currentTeam().isDefeated() ):
				break

		if gameOver:
			log.msg("Game over!")
		
		o = self.onTurn
		self.onTurn = defer.Deferred()
		o.callback(self.getTurn())
		

	def create(self, mapid, entityset, creator):
		"""
		Create a new game in the database
		"""
		log.msg("create game")
		assert self.gameid == None
		#TODO: verify mapid
		#TODO: verify entityset
		self.map = map.getMap(mapid)
		self.mapid = mapid
		self.entityset = entityset
		self.creator = creator
		self.all_entities = {}
		self.teams = {}
		self.turn = 1
		self.history = []
#Team(self, data={"teamid":x, "resources":0, "userid":None}) for x in self.playersInGame()[1] ]

		ets = map.getEntityset(entityset)
		for e in ets["entities"]:
			if e["teamid"] not in self.teams:
				team = e["teamid"]
				self.teams[team] = Team(self, data={
					"teamid":team,
					"resources":0,
					"userid":None,
					"defeated":False
					})
			ent = self.spawnEntity( copy.deepcopy(e) )

		for ent in self.all_entities.values():
			ent.newRound()
			ent.newDay()

	def getTeams(self):
		"""
		Return a list of teams, indexed by their teamid - 1.
		"""
		r = [None]*len(self.teams)
		for k,v in self.teams.iteritems():
			r[k - 1] = v.getData()
		return r

	def getDay(self):
		"""
		Returns amount of full turn cycles (counting from 1).
		"""
		return ((self.turn - 1) / len(self.teams)) + 1

	def getTurn(self):
		"""
		Return index of current player in range [1, len(teams)].
		"""
		return ((self.turn-1) % len(self.teams)) + 1

	def getEntities(self):
		"""
		Returns list (indexed by teamid - 1) of dict<entityid, entity>.
		"""
		r = [None]*len(self.teams)
		for tid in xrange(1, len(self.teams) + 1):
			r[tid - 1] = {}
			for k,v in self.teams[tid].getEntities().iteritems():
				r[tid - 1][k] = v.getData()
		return r


	def getId(self):
		assert self.gameid
		return self.gameid

	def getMapid(self):
		return self.mapid

	def sendCommand(self, user, entityid, command, params):
		log.msg("command: " + command + " " + str(params))
		if not self.currentTeam().isUser(user):
			raise TurnError()
		try:
			e = self.currentTeam().getEntities()[ entityid ]
		except KeyError:
			raise JSONRPCError("no such entity: ")
		r = e.message(command, params)
		return r

	def getNewEntityId(self):
		for tries in xrange(0, int(math.pow(2,32))):
			i = int(random.randint(0, (math.pow(2,32)-1)) - math.pow(2,32)/2)
			if i not in self.all_entities:
				return i
		raise InternalError(responseinfo="Failed to make entityid")


	def spawnEntity(self, data):
		"""
		Create a new entity locally and in the database.
		"""
		#TODO: Add to history
		data["entityid"] = self.getNewEntityId()
		entity = Entity(self, data)
		entity.onSpawn()
		self.loadEntity(entity)
		return entity

	def show(self):
		teamColors = ["red", "blue", "green", "cyan"]
		colors = self.map.getColors()
		for e in self.all_entities.itervalues():
			if not e.isDestroyed():
				pos = colors[ e["ypos"]][e["xpos"]]
				pos["char"] = e.specs["namechar"]
				pos["fg"] = teamColors[ e["teamid"] - 1 ]
				if pos["bg"] == pos["fg"]:
					pos["bg"] = "white"

		for line in colors:
			out = ""
			for col in line:
				try:
					out += colored(col["char"], col["fg"], "on_" + col["bg"], attrs=col["attr"])
				except:
					out += col["char"]

			print out

		


				
