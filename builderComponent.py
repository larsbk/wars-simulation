from component import Component
from dataObject import *
from warsexceptions import *
import game
import entity
from twisted.python import log

class componentBuilder(Component):
	def __init__(self, parent, specs=dict()):
		super(componentBuilder, self).__init__(parent, specs)
		parent.actions["build"] = self.build

	def build(self, req):
		if req["entitytype"] not in self.specs["types"]:
			raise ParamError(param="entitytype", responseinfo="Unable to build this type")

		ent = None
		s = self.specs["types"][req["entitytype"]]
		self.parent.getPlayer().makeBackup()
		try:
			self.parent.getPlayer().subtractValue("resources", s["cost"])
			if "timecost" in s:
				self.parent.subtractValue("time", s["timecost"])
			d = dict()
			d["teamid"] = self.parent.data["teamid"]
			d["xpos"]= self.parent.data["xpos"]
			d["ypos"] = self.parent.data["ypos"]
			d["type"] = req["entitytype"]
			ent = self.parent.game.spawnEntity(d)

		except DataResourceError as e:
			self.parent.getPlayer().rollback()
			raise
		
		self.parent.game.newData(entities=[self.parent, ent], participants=[self.parent.getPlayer()])
		

