from component import Component
import random
from twisted.python import log
def hitMultiplier(critMul, critSize, hitSize, partialSize, missSize):
	total = critSize + hitSize + partialSize + missSize
	critSize /= total
	hitSize /= total
	partialSize /= total
	missSize /= total
	
	r = random.random()
	if r <= missSize:
		return 0
	r -= missSize
	if r <= partialSize:
		return (r/partialSize)
	r -= partialSize
	if r <= hitSize:
		return 1
	r -= hitSize
	return (r/critSize)

class componentHealth(Component):
	
	def __init__(self, parent, specs=dict()):
		super(componentHealth, self).__init__(parent, specs)
		parent.all_tables.add("entities")

	def applyDamage(self, damage):
		if not self.parent.isDestroyed():
			self.parent.data["health"] -= damage
			if(self.parent.data["health"] < 0):
				self.parent.data["health"] = 0

	def calculateDamage(self, typeList, damage, partialprob, hitprob):
		dam = damage
		tile = self.parent.getTile()
		prob = hitprob * self.specs["hitprob"] * tile["hitprob"]
		log.msg("prob: %f" % prob)
		mul = hitMultiplier(2, self.specs["critprob"], prob, partialprob, 1-(partialprob+prob))
		log.msg("mul:  %f" % mul)
		for t in typeList:
			if t in self.specs["vulnerability"]:
				mul *= self.specs["vulnerability"][t]
		log.msg("mul:  %f" % mul)
		return dam * mul


		
