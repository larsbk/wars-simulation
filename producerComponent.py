from component import Component
from twisted.python import log

class componentProducer(Component):

	def __init__(self, parent, specs=dict()):
		super(componentProducer, self).__init__(parent, specs)
	
	def newDay(self):
		log.msg("producer newDay!")
		self.parent.getPlayer().makeBackup()
		try:
			self.parent.getPlayer().increaseValue("resources",
				self.specs["resources"]*self.parent.data["health"] )
		except DataError:
			self.parent.getPlayer().rollback()
			raise


	

