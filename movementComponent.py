from component import Component
from twisted.python import log
from txjason.service import JSONRPCError

class componentMovement(Component):
	
	def __init__(self, parent, specs=dict()):
		super(componentMovement, self).__init__(parent, specs)
		parent.actions["move"] = self.move
		parent.all_tables.add("units")

	def onSpawn(self):
		return self.legalPos(True, self.parent.data["xpos"], self.parent.data["ypos"])

	def legalPos(self, stand, x, y):
		if (x < 0 or x >= self.parent.map.data["width"] or
			y < 0 or y >= self.parent.map.data["height"]):
			return False
		tile = self.parent.getTileAt(x,y)
		if tile["water"] > self.specs["maxwater"]:
			return False
		if tile["terrain"] > self.specs["maxterrain"]:
			return False
		o = self.parent.getEntitiesAtPos(x, y)
		for u in o:
			if "Unit" in u.components and (not u.isDestroyed()):
				if stand == True:
					return False
				elif u.data["teamid"] != self.parent.data["teamid"] and u.data["teamid"] >= 0 :
					return False
				
		return True

	
	def move(self, message):
		for direction in message["direction"]:
			(x,y) = self.parent.getPosFromDirection(direction)
			log.msg("map %s" % str(self.parent.map))
			tile = self.parent.getTileAt(x,y)
			if tile == None:
				raise JSONRPCError("out of bounds")
			if not self.legalPos(False, x,y):
				raise JSONRPCError("illegal pos")

			self.parent.subtractValue("time", self.specs["watertimecost"]*tile["water"] \
					+ self.specs["terraintimecost"]*tile["terrain"])

			self.parent.subtractValue("fuel", self.specs["waterfuelcost"]*tile["water"] \
					+ self.specs["terrainfuelcost"]*tile["terrain"])
			self.parent.data["xpos"] = x
			self.parent.data["ypos"] = y

			self.parent.changed_tables.add("units")
			self.parent.changed_tables.add("entities")

		if not self.legalPos(True, self.parent.data["xpos"], self.parent.data["ypos"]):
			raise JSONRPCError("illegal pos")

		self.parent.game.newData(entities=[self.parent,])




